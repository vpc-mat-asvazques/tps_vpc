# Trabajo práctico nro. 7

El objetivo de este práctico es entrenar un detector de objetos en imágenes usando la red Yolo.
Para probar el modelo:
* Buscar 2 imágenes nuevas,
* mostrarlas en pantalla (matplotlib),
* detectar objetos con el modelo
* y mostrar el resultado de la detección (recuadro que indique la ubicación y clase del objeto).

Usar template disponible en colab.

## Desarrollo

Se desarrolló el trabajo práctico en Colab, para poder tener el resultado dentro de ese archivo y facilitar la correción. El archivo es: 

```
tp7.ipynb
  ```

El modelo creado es capaz de detectar gatas carey, las cuales poseen un tipo de manchas distintivo: blanco, negro y naranja. El modelo es el siguiente: 

```
yolov4-custom_last.weights
  ```