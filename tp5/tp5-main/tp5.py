import cv2
import numpy as np
import yaml

# Es necesario crear una sentencia ejecutable definida por el usuario, 
# en donde se obtendrá la matriz de cámara, coeficientes de distorsión, vectores de rotación y 
# traslación.

#matrix_data = data


cap =cv2.VideoCapture("Test.mp4")
#cap.set(3, 640)
#cap.set(4, 480)

saving = 0
outputFile = "muestraTP5.avi"
vid_writer = cv2.VideoWriter(outputFile, cv2.VideoWriter_fourcc('M','J','P','G'), 120, (round(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),round(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))))

cv2.namedWindow('Trabajo Practico Nro. 5')

def obj_detection(frame):
    
    # convertimos la imagen a escala de grises
    # crear una mascara con umbral adaptativo
    # buscamos los contornos 
    # se crea una lista donde se almacenaran los objetos
    
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    mask = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 19, 5)
    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    obj_contours = []
    
    for contour in contours: 
                
        # Medimos el area de los contornos
        area = cv2.contourArea(contour)
        
        if area > 2000: 
            obj_contours.append(contour)
            
    return obj_contours
            
            
       
def get_cam_matrix(file): 
    with open(file) as f:
        loadeddict = yaml.safe_load(f)
        cam_matrix = np.array(loadeddict.get('camera_matrix'))
        dist_coeff = np.array(loadeddict.get('dist_coeff'))
        rvecs = np.array(loadeddict.get('rvecs'))
        tvecs = np.array(loadeddict.get('tvecs'))
        return cam_matrix,dist_coeff,rvecs,tvecs

cam_matrix, dist_coefs, rvecs, tvecs = get_cam_matrix("data1.yaml")

# rvects: rotacion
# tvects: traslacion

while (True):
    ret, frame = cap.read()
    k = cv2.waitKey(1) & 0xFF
    if k == ord('q'):
        break 
    
    frame_copy = frame.copy()
    
    ArUco_dict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_6X6_250)
        
    parameters =  cv2.aruco.DetectorParameters_create()
        
    markerCorners, markerIds, rejectedCandidates = cv2.aruco.detectMarkers(frame, ArUco_dict, parameters=parameters)    
    markerCorners_ent = np.int0(markerCorners)
    #cv2.polylines(frame, markerCorners_ent, True, (255,0,255), 5)
    
    perimetroAruco = cv2.arcLength(markerCorners_ent[0], True)
    proporcion_cm = perimetroAruco / 16 # 16 es el perimetro del aruco real, 4cmx4cm    
    
    contornos = obj_detection(frame)
         
    for contorno in contornos: 
        # se puede dibujar el contorno del objeto
        # cv2.polylines(frame, [cont], True, (0,255,0, 2))
        
        # Rectangulo del objeto: 
        # A partir del anterior poligono se obtiene un rectángulo 
        rectangle = cv2.minAreaRect(contorno)
        (x,y), (wid,hei), angulo = rectangle
    
        # Hay que pasar el ancho y el alto de pixeles a centímetros

        width = wid / proporcion_cm
        height = hei / proporcion_cm

        # Se dibuja un circulo en la mitad del rectangulo
    
        cv2.circle(frame, (int(x), int(y)), 5, (255,0,255), -1)

        # Se dibuja el rectangulo que se obtuvo

        rect = cv2.boxPoints(rectangle)
        rect = np.int0(rect)

        # Dibujamos el rectangulo}

        cv2.polylines(frame, [rect], True, (255, 0, 255), 2)

        # Se muestra la info en pixeles

        cv2.putText(frame, "Ancho: {} cm".format(round(width,1)), (int(x), int(y-15)), cv2.LINE_AA, 0.8, (0, 128, 255), 2)
        cv2.putText(frame, "Largo: {} cm".format(round(height,1)), (int(x), int(y+15)), cv2.LINE_AA, 0.8, (255, 255, 0), 2)
        
        #concatenatedOutput = cv2.hconcat([frame_copy.astype(np.uint8), frame.astype(np.uint8)])
        vid_writer.write(frame.astype(np.uint8))
                
    cv2.imshow('Trabajo Practico Nro. 5',frame)
    
    #if markerIds is not None:
    #    for i, corner in zip(markerIds, markerCorners):
    #        print('ID: {}; corners :{}; perimetroAruco: {}'.format(i,corner,perimetroAruco))
    #        frame = cv2.aruco.drawDetectedMarkers(frame,markerCorners,borderColor=(0,255,0))
    
    ### Estimación de pose ###
    
    #if markerIds is not None and markerCorners is not None:
    #    rvecs, tvecs, _= cv2.aruco.estimatePoseSingleMarkers(markerCorners, 0.05 , cam_matrix, dist_coefs)

    # se realiza la estimación de pose con la función aruco.estimatePoseSingleMarkes, la cual necesita 4 parámetros:
    # corners
    # longitud de los vectores
    # matriz de la camara
    # coeficiente de distorsion 
    
    #for i in range (0, len(rvecs)):
    #    cv2.drawFrameAxes(frame, cam_matrix , dist_coefs , rvecs[i], tvecs[i], 0.1)
        
    #cv2.imshow('frame',frame)
    

cap.release()
cv2.destroyAllWindows()
