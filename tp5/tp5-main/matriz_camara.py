import numpy as np
import cv2
import glob
import yaml

#Criterio de terminación de iteración 

criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

#Luego se determina el tamaño del tablero de ajedrez, como ya se había especificado anteriormente 
#es de 9x6.

width, height = 9, 6
objp = np.zeros((width * height,3), np.float32)
objp[:,:2] = np.mgrid[0:width,0:height].T.reshape(-1,2)

# Y se crean las variables para el conjunto de puntos 3D (objpoints), los puntos de imagen 2D 
# (imgpoints) y la variable que almacenara todos los datos posteriormente (data), además se cargan 
# las imágenes de muestra tomadas al patrón de ajedrez.

objpoints = [] 
imgpoints = [] 
data=None
images = glob.glob('./ChessBoardSet/*.jpg')

for fname in images:
    img = cv2.imread(fname)
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    w, h = img.shape[:2]
    ret, corners = cv2.findChessboardCorners(gray, (width, height ),None)

# Si la función findChessboardCorrners devuelve un valor diferente de cero entonces, se puede mejorar su precisión utilizando 
# cv2.cornerSubPix (), y también se pueden dibujar y guardar los patrones sobre las imágenes 
# tomadas con cv2.drawChessboardCorners ().

if ret == True:
    objpoints.append(objp)
    corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
    imgpoints.append(corners2)
    imgName = fname[fname.rindex('\\') + 1 : ]
    img = cv2.drawChessboardCorners(img, (width ,height), corners2,ret)
    cv2.imwrite('./ChessBoardMarkedSet/' + imgName, img)
    
# el siguiente paso es realizar la calibración con la función, 
# cv2.calibrateCamera ().
    
ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)

newcameramtx, roi=cv2.getOptimalNewCameraMatrix(mtx,dist, (w,h), 1, (w,h))

if ret:
    data = {'camera_matrix': np.asarray(newcameramtx).tolist(),
    'dist_coeff': np.asarray(dist).tolist(),
    'rvecs': np.asarray(rvecs).tolist(), 
    'tvecs': np.asarray(tvecs).tolist()}
with open('data1.yaml', 'w') as f:
    yaml.dump(data,f)
print(data) 
cv2.destroyAllWindows()

