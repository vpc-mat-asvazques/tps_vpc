import cv2 as cv
import numpy as np
 
# Carga de un diccionario predefinido
dictionary = cv.aruco.Dictionary_get(cv.aruco.DICT_6X6_250)
 
# Generación del marcador
markerImage = np.zeros((200, 200), dtype=np.uint8)
markerImage = cv.aruco.drawMarker(dictionary, 23, 200, markerImage, 1);
 
cv.imwrite("marker23.png", markerImage);
