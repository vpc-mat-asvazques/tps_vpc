# Trabajo Práctico Nro. 5: Práctico libre ArUCo

## Consigna:

En base al teórico de ArUCo y a los videos presentados, se propone realizar una aplicación simple en base a ArUCo. Puede ser por ejemplo realidad virtual en 2D o en 3D, estimación de posición y orientación de objetos en movimiento, seguimiento de objetos, o
simplemente hacer instalar algún repositorio interesante y aplicarlo a algo.

https://www.youtube.com/watch?v=nsu9tNIJ6F0

https://youtu.be/VsIMl8O_F1w?t=55

https://www.youtube.com/watch?v=qqDXwKDr0vE

https://www.youtube.com/watch?v=RNwTGPvuhPw&t=8

https://www.youtube.com/watch?v=xzG2jQfxLlY

https://www.youtube.com/watch?v=jwu9SX3YPSk

https://www.youtube.com/watch?v=FBl4Y55V2Z4

El puntaje del práctico dependerá de la complejidad, utilidad o nivel de innovación de la aplicación.

## Desarrollo

### Generación de los marcadores 

Se desea implementar un marcador de 6x6, que tiene la posibilidad de utilizar 250 elementos. Con lo que se utiliza la siguiente función: 

```py
dictionary = cv.aruco.Dictionary_get(cv.aruco.DICT_6X6_250)
 ```

Luego se crea una imagen de 200 x 200 px y con la método drawmarker se le asigna el patrón correspondiente al marcador con id 23. Se realiza el mismo procedimiento para cuatro marcadores extra.

```
# Generación del marcador
markerImage = np.zeros((200, 200), dtype=np.uint8)
markerImage = cv.aruco.drawMarker(dictionary, 23, 200, markerImage, 1);
 
cv.imwrite("marker23.png", markerImage);
  ```

El marcador 23: 

![Marcador ID 23](marker23.png)

### Calibración de la cámara 

Luego de generar el marcador se debe realizar la calibración de la camara con:

```
python3 matriz_camara.py
  ```

### Implementación 

El trabajo que se ideó es utilizado para realizar mediciones de manera análoga al trabajo práctico nro. 4. La referencia es el marcador ARUCO con dimensiones conocidas, en este caso es de 4cm, con esta referencia se detectan objetos sobre el mismo plano y se realiza la conversión de pixeles del perímetro rectangular de cada objeto a centímetros. 
Se muestra acontinuación el resultado, luego de ejecutar en consola el script: 

```
python3 tp5.py
  ```

![Resultado](resultado.png)

El video de test es: 

```
Test.mp4
  ```

Y el video de muestra es: 

```
muestraTP5.avi
  ```