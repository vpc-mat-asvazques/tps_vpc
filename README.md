# Trabajos Prácticos de Visión por Computadora

## Trabajo Práctico Nro. 1: Eventos y callbacks

### Consigna: 
Usando como base el programa brindado por la cátedra, crear un programa que permita seleccionar una
porción rectangular de una imagen con el ratón, luego:
- con la letra “g” lo guardamos a disco en una nueva imagen y salimos.
- con la letra “r” restauramos la imagen original y volvemos a realizar la selección.
- con la “q” salimos.

Para correr el programa colocar en la consola:
```sh
  python3 tp1.py
  ```

### Desarrollo

Se utilizará como imagen base la pantalla de selección del juego Super Smash Bros porque en la misma se presentan bordes definidos entre cada personaje, con lo que se podrá verificar si la selección, recorte y guardado son correctos o no.

![Selección de personaje](tp1-main\tp1-main\image.jpg)

Aquí debajo se observará la última selección realizada: 

![Selección de personaje](tp1-main\tp1-main\resultado_tp1.png)

## Trabajo Práctico Nro. 2: Transformación afín - Incrustando imágenes

### Consigna: 
Teniendo en cuenta que:
- Una transformación afín se representa con una matriz de 2 × 3.
- Tiene 6 grados de libertad y puede ser recuperada con 3 puntos.

Usando como base el programa escrito en la práctica 1, se pide: 

- Crear un programa que permita seleccionar con el ratón 3 puntos de una primera imagen.
- Luego crear un método que compute una transformación afín entre las esquinas de
una segunda imagen y los 3 puntos seleccionados.
- Por último aplicar esta transformación sobre la segunda imagen, e incrustarla en la
primera imagen.

Ayuda: 

```sh
  cv2.getAffineTransform
  ```
```sh
  cv2.warpAffine
  ```

Para correr el programa colocar en la consola:
```sh
  python3 tp2.py
  ```

Para salir del programa presione "Q".

### Desarrollo

Se utilizará como imagen base:

![Imagen destino](tp2-main\tp2-main\DestinoDST.jpg)

La imagen que se transformará es: 

![Imagen fuente](tp2-main\tp2-main\FuenteSRC.png)

Se crea un fondo con el fondo negro que se combinará con la imagen transformada, de otra forma se superpondrían: 

![Imagen fuente](tp2-main\tp2-main\back.jpg)

La imagen fuente transformada es: 

![Imagen fuente](tp2-main\tp2-main\front.jpg)

Finalmente, a continuación se observa el resultado de la aplicación de la transformada: 

![Resultado](tp2-main\tp2-main\resultado_tp2.jpg)

## Trabajo Práctico Nro. 3: Rectificando imágenes

### Consigna: 
Teniendo en cuenta que:
- Una homografía se representa con una matriz de 3 × 3.
- Tiene 8 grados de libertad y puede ser recuperada con 4 puntos 

Usando como base el programa anterior, se pide:

- Crear un programa que permita seleccionar 4 puntos de una primera imagen.
- Luego crear un método que compute una homografía entre dichos 4 puntos y una
segunda imagen estándar de m × n pixeles.
- Por último aplicar esta transformación para llevar (rectificar) la porción de la primera
imagen definida por los 4 puntos elegidos a la segunda de m × n.

Ayuda: 

```sh
  cv2.getPerspectiveTransform
  ```
```sh
  cv2.warpPerspective
  ```

Para correr el programa colocar en la consola:
```sh
  python3 tp3.py
  ```

Para salir del programa luego de la selección de los cuatro puntos presionar "Q".

### Desarrollo

Se utilizará como imagen base:

![Imagen destino](tp3-main\tp3-main\SRC.jpg)

A continuación se observa el resultado de la aplicación de la transformada: 

![Resultado](tp3-main\tp3-main\resultado_tp3.jpg)
# Trabajo Práctico Nro. 4: Medición de objetos sobre plano calibrado

## Consigna: 

La siguiente imagen es una foto de la fachada de una casa.

![Imagen destino](images\fachada.jpg "Foto de ejemplo. Sacar una foto propia.")

Tomando esta foto como ejemplo y considerando como dato conocido el tamaño del vano de la puerta, 2.10m de alto por 0.73m de ancho, se pueden determinar las dimensiones de cualquier objeto de la imagen siempre que esten en el mismo plano de la fachada.

Como práctica del tema se pide:

- Capturar una imagen propia con perspectiva, que puede ser de una fachada o cualquier otra escena plana, que tenga un objeto rectangular de dimensiones conocidas,
- escribir un programa que permita encontrar la transformación perspectiva entre los 4 vértices del objeto en la imagen y un rectángulo con la misma relación de aspecto que el objeto real,
- que luego de elegir los 4 vértices aplique dicha transformación a la imagen y la muestre en una nueva ventana “calibrada para medición”,
- sobre esta ventana de medición el programa debe permitir que el usuario elija con el ratón dos puntos cualquiera y debe mostrar la distancia en metros entre dichos puntos;
- la ventana de medición debe poder ser restaurada cuando se presione la tecla “r” para realizar una nueva medición;
- por último medir dos objetos en la imagen (ventana, casilla del gas, etc.) y verificar los resultados con la medida real.

Para correr el programa colocar en la consola:
```sh
  python4 tp4.py
  ```

Para salir del programa luego de la selección de los cuatro puntos presionar "Q", para guardar la imagen con las mediciones realizadas debe presionar "G".

## Desarrollo

Se utilizará como imagen base:

![Imagen destino](fachada.jpeg)

A continuación se observa el resultado de la aplicación de la transformada: 

![Resultado](resultado_tp4.png)

# Trabajo Práctico Nro. 5: Medición de objetos sobre plano calibrado

## Consigna:

En base al teórico de ArUCo y a los videos presentados, se propone realizar una aplicación simple en base a ArUCo. Puede ser por ejemplo realidad virtual en 2D o en 3D, estimación de posición y orientación de objetos en movimiento, seguimiento de objetos, o
simplemente hacer instalar algún repositorio interesante y aplicarlo a algo.

https://www.youtube.com/watch?v=nsu9tNIJ6F0

https://youtu.be/VsIMl8O_F1w?t=55

https://www.youtube.com/watch?v=qqDXwKDr0vE

https://www.youtube.com/watch?v=RNwTGPvuhPw&t=8

https://www.youtube.com/watch?v=xzG2jQfxLlY

https://www.youtube.com/watch?v=jwu9SX3YPSk

https://www.youtube.com/watch?v=FBl4Y55V2Z4

El puntaje del práctico dependerá de la complejidad, utilidad o nivel de innovación de la aplicación.

## Desarrollo

### Generación de los marcadores 

Se desea implementar un marcador de 6x6, que tiene la posibilidad de utilizar 250 elementos. Con lo que se utiliza la siguiente función: 

```py
dictionary = cv.aruco.Dictionary_get(cv.aruco.DICT_6X6_250)
 ```

Luego se crea una imagen de 200 x 200 px y con la método drawmarker se le asigna el patrón correspondiente al marcador con id 23. Se realiza el mismo procedimiento para cuatro marcadores extra.

```
# Generación del marcador
markerImage = np.zeros((200, 200), dtype=np.uint8)
markerImage = cv.aruco.drawMarker(dictionary, 23, 200, markerImage, 1);
 
cv.imwrite("marker23.png", markerImage);
  ```

El marcador 23: 

![Marcador ID 23](marker23.png)

Luego de generar el marcador se debe realizar la calibración de la camara con:

```
python3 matriz_camara.py
  ```

El trabajo que se ideó es utilizado para realizar mediciones de manera análoga al trabajo práctico nro. 4. La referencia es el marcador ARUCO con dimensiones conocidas, en este caso es de 4cm, con esta referencia se detectan objetos sobre el mismo plano y se realiza la conversión de pixeles del perímetro rectangular de cada objeto a centímetros. 
Se muestra acontinuación el resultado, luego de ejecutar en consola el script: 

```
python3 tp5.py
  ```

![Resultado](resultado.png)

El video de test es: 

```
Test.mp4
  ```

Y el video de muestra es: 

```
muestraTP5.avi
  ```