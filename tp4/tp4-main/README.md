# Trabajo Práctico Nro. 4: Medición de objetos sobre plano calibrado

## Consigna: 

La siguiente imagen es una foto de la fachada de una casa.

![Imagen destino](images\fachada.jpg "Foto de ejemplo. Sacar una foto propia.")

Tomando esta foto como ejemplo y considerando como dato conocido el tamaño del vano de la puerta, 2.10m de alto por 0.73m de ancho, se pueden determinar las dimensiones de cualquier objeto de la imagen siempre que esten en el mismo plano de la fachada.

Como práctica del tema se pide:

- Capturar una imagen propia con perspectiva, que puede ser de una fachada o cualquier otra escena plana, que tenga un objeto rectangular de dimensiones conocidas,
- escribir un programa que permita encontrar la transformación perspectiva entre los 4 vértices del objeto en la imagen y un rectángulo con la misma relación de aspecto que el objeto real,
- que luego de elegir los 4 vértices aplique dicha transformación a la imagen y la muestre en una nueva ventana “calibrada para medición”,
- sobre esta ventana de medición el programa debe permitir que el usuario elija con el ratón dos puntos cualquiera y debe mostrar la distancia en metros entre dichos puntos;
- la ventana de medición debe poder ser restaurada cuando se presione la tecla “r” para realizar una nueva medición;
- por último medir dos objetos en la imagen (ventana, casilla del gas, etc.) y verificar los resultados con la medida real.

Para correr el programa colocar en la consola:
```sh
  python4 tp4.py
  ```

Para salir del programa luego de la selección de los cuatro puntos presionar "Q", para guardar la imagen con las mediciones realizadas debe presionar "G".

## Desarrollo

Se utilizará como imagen base:

![Imagen destino](fachada.jpeg)

A continuación se observa el resultado de la aplicación de la transformada: 

![Resultado](resultado_tp4.png)

