#! /usr/bin/env python
# -*- coding: utf-8 -*-

import cv2
import numpy as np

drawing = 0
coordenadas = []
m_height, m_width = 2.1, 1.5

img = cv2.imread('fachada.jpeg')
scale_percent = 50 # percent of original size
width = int(img.shape[1] * scale_percent / 100)
height = int(img.shape[0] * scale_percent / 100)
dim = (width, height)
# resize image
img = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)

img_base = img.copy()
#print(img.shape[:2])
perspective_base = np.zeros((280, 820), dtype=int)

#print(img.shape[:2])
dst = perspective_base

rowsSrc, colsSrc = dst.shape[:2]

#output_pts = np.float32([[300,rowsSrc-140], [400,rowsSrc-140], [400,rowsSrc-1], [300,rowsSrc-1]]) 
output_pts = np.float32([[300,rowsSrc-210], [450,rowsSrc-210], [450,rowsSrc-1], [300,rowsSrc-1]]) 

px_height = 210 #ancho de la imagen real en px
px_width = 150 #alto de la imagen real en px  

#Esquinas del recuadro [[Izq+Arriba],[Der+Arriba],[Der+Abajo],[Izq+Abajo]]
input_pts = []

def get_measure(event, x, y, flags, param):
 global drawing, perspective_base
 
 if event == cv2.EVENT_LBUTTONDOWN:
  global dst  
  coordenadas.append([x,y]) 
  
  #print(x,y)
  drawing += 1
  cv2.circle(dst, (x, y), 3 ,(0, 0, 255), -1)
  
 if drawing == 2: 
  text_posx, text_posy = coordenadas[0]
  global height, width
  
  pos1x, pos1y = coordenadas[0]
  pos2x, pos2y = coordenadas[1]
  
  difx = abs(pos2x - pos1x)
  dify = abs(pos2y - pos1y)
  
  x = ((difx * m_width) / px_width)/1
  y =   ((dify * m_height) / px_height)/1
  
  
  measure = np.sqrt(pow(x,2)+pow(y,2))
  
  cv2.line(dst,(coordenadas[0]),(coordenadas[1]),(0,0,255),2)
  font = cv2.FONT_HERSHEY_SIMPLEX
  cv2.putText(dst,"{:.2f}m".format(measure),(text_posx-10,text_posy-10),font,0.8,(255,255,255),2,cv2.LINE_AA)
  drawing = 0 
  coordenadas.clear()
  
  

def reset_measure():
  global dst, perspective_base
  dst = perspective_base.copy()

def perspective(): 
  global dst, perspective_base
  #input_pts = np.float32([[298,188],[392,189],[393,312],[297,310]])
  input_pts = np.float32([[298,188],[392,189],[393,312],[297,310]])
  M = cv2.getPerspectiveTransform(input_pts, output_pts)
  perspective_base = cv2.warpPerspective(img_base.copy(), M, (colsSrc,rowsSrc))
  
  dst = perspective_base.copy() 
  perspective_base = dst.copy()
   
def save_image():
 global dst
 cv2.imwrite('resultado_tp4.png', dst) 
 #print("guardado")

cv2.namedWindow('Original')
cv2.namedWindow('Calibrada para medicion')
perspective()
cv2.setMouseCallback('Calibrada para medicion', get_measure)

while(1):
 cv2.imshow('Original', img)
 cv2.imshow('Calibrada para medicion', dst)
 k = cv2.waitKey(1) & 0xFF
 if k == ord('r'):
  reset_measure()
 if k == ord('g'):
  save_image()
 if k == ord('q'):
  break
cv2.destroyAllWindows()
