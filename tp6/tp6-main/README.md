# Trabajo Práctico Nro. 6: Clasificación de imágenes usando CNN

## Consigna:

En este práctico vamos a implementar un clasificador de imágenes usando CNN. El objetivo será diseñar y entrenar un algoritmo que en forma automática clasifique una imagen como correspondiente a una clase entre varias. Las clases las vamos a elegir nosotros (perros
y gatos, autos y motos, o algo más interesante o útil).
Para probar el modelo, se deben buscar 2 imágenes nuevas, mostrarlas en pantalla (matplotlib), clasificarlas con el modelo y mostrar el resultado de la clasificación (puntaje para cada clase).
Usar plantilla disponible en colab.

## Desarrollo

Se desarrolló el trabajo práctico en Jupiter Notebooks, para poder tener el resultado dentro de ese archivo y facilitar la correción. El archivo es: 

```
tp6.ipynb
  ```